package com.dockerforjavadevelopers.hello;

// import static org.junit.Assert.*;

// import org.junit.Test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class DummyTest {

  // @Test
  // public void aTest() {
  //   assertEquals(true, true);
  // }

  @Autowired
    private TestRestTemplate template;

    @Test
    public void hello_ok() throws Exception {
        ResponseEntity<String> response = template.getForEntity("/", String.class);
        assertThat(response.getBody()).isEqualTo("Hello World, My name is Saravannan Ponmudi \n");
    }

    @Test 
    public void main(){
      Application.main(new String[] {});
    }
}
